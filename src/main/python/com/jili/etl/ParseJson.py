#!/usr/bin/python
#-*- encoding=utf-8 -*-

import sys
import json

reload(sys)
sys.setdefaultencoding("utf-8")

#json_file = open("./resources/InputFile.json", "r")

def helloworld():
    print "Python: Hello World!"

def read(obj,key):
    collect = list()
    for k in obj:
        v = obj[k]
        if isinstance(v,dict):
            collect.extend(read(v,k))
        elif isinstance(v,list):
            if key=='':
                collect.extend(read_list(v,k))
            else:
                collect.extend(read_list(v,key+"."+k))
        else:
            if key=='':
                collect.append({k:v})
            else:
                collect.append({str(key)+"."+k:v})
    return collect

def read_list(obj,key):
    collect = list()
    for index,item in enumerate(obj):
        #print "index = %s" % index
        #print "item = %s" % item
        if isinstance(item,dict):
            for k in item:
                v = item[k]
                if isinstance(v,dict):
                    collect.extend(read(v,key+"["+str(index)+"]"))
                elif isinstance(v,list):
                    collect.extend(read_list(v,key+"["+str(index)+"]"))
                else:
                    collect.append({key+"["+str(index)+"]"+"."+k:v})
        else:
            v = item
            if isinstance(v,dict):
                collect.extend(read(v,key+"["+str(index)+"]"))
            elif isinstance(v,list):
                collect.extend(read_list(v,key+"["+str(index)+"]"))
            else:
                collect.append({key+"["+str(index)+"]":v})
    return collect

def read_jsonstr(jsonstr):
    jsonobj = json.loads(jsonstr)
    result = read(jsonobj,'')
    #print result
    return result

#if __name__ == "__main__":
#    try:
#        while True:
#            line = json_file.readline()
#            if not line:
#                break
#            jsonobj = json.loads(line)
#            result = read(jsonobj,'')
#    except Exception, exception:
#        print exception
