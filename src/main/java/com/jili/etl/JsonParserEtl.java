package com.jili.etl;

import org.python.core.Py;
import org.python.core.PyFunction;
import org.python.core.PyObject;
import org.python.core.PyString;
import org.python.util.PythonInterpreter;

import java.io.*;
import java.net.URL;

/**
 * Created by bill on 2016/9/11.
 */
public class JsonParserEtl {

    public static File getResFile(String FullFileName){
        URL res = JsonParserEtl.class.getResource(FullFileName);

        File TmpFile = null;
        if (res.toString().startsWith("jar:")) {
            try {
                InputStream pythonIS = JsonParserEtl.class.getResourceAsStream(FullFileName);
                TmpFile = File.createTempFile("TmpFile", ".tmp");
                OutputStream out = new FileOutputStream(TmpFile);
                int read;
                byte[] bytes = new byte[1024];
                while ((read = pythonIS.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }
                TmpFile.deleteOnExit();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            // this will probably work in your IDE, but not from a JAR
            TmpFile = new File(res.getFile());
        }

        if (TmpFile != null && !TmpFile.exists()) {
            throw new RuntimeException("Error: File " + TmpFile + " not found!");
        }
        return TmpFile;
    }

    public static void main(String[] args){
        String pythonFile = "/com/jili/etl/ParseJson.py";
        File pyFile = getResFile(pythonFile);

        FileInputStream pyFileInputStream = null;
        try {
            pyFileInputStream = new FileInputStream(pyFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        PythonInterpreter interpreter = new PythonInterpreter();
        interpreter.execfile(pyFileInputStream);

        PyFunction helloFunc = interpreter.get("helloworld", PyFunction.class);
        helloFunc.__call__();

        String jsonFile = "/InputFile.json";
        File jsonResFile = getResFile(jsonFile);
        PyFunction parseFunc = interpreter.get("read_jsonstr", PyFunction.class);
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(jsonResFile));
            String tempString = null;
            while ((tempString = reader.readLine()) != null) {
                //PyObject pyobj = parseFunc.__call__(new PyString(tempString));
                PyObject pyobj = parseFunc.__call__(Py.newStringOrUnicode(tempString));
                System.out.println(pyobj.toString());
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                }
            }
        }

        try {
            pyFileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
